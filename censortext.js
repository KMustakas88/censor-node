var censoredWords = ["sad", "bad", "mad"];
var customCensoredWords = [];
function censor(inStr) {
  for (idx in censoredWords) {
    inStr = inStr.replace(censoredWords[idx], "****");
  }
  for (idx in customCensoredWords) {
    inStr = inStr.replace(customCensoredWords[idx], "****");
  }
  return inStr;
}
function addCenosredWord(word) {
  customCensoredWords.push(word);
  }
function getCensoredWords() {
  return censoredWords.concat(customCensoredWords);
}
exports.censor = censor;
exports.addCenosredWord = addCensorWord;
exports.getCensoredWords = getCensoredWords;
